// ***************************************************************
// This file was created using the CreateProject.sh script.
// CreateProject.sh is part of Bayesian Analysis Toolkit (BAT).
// BAT can be downloaded from http://www.mppmu.mpg.de/bat
// ***************************************************************

#ifndef __BAT__LIFETIMEMODEL__H
#define __BAT__LIFETIMEMODEL__H

#include <BCModel.h>
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "DataFactory.h"
#include "TMath.h"

#include <unordered_map>
#include <functional>
// This is a LifetimeModel header file.
// Model source code is located in file Lifetime_Phase0.5/LifetimeModel.cxx

// class CompVec
// {
// public :
//   bool operator() (const std::vector<double>& a,const std::vector<double>& b) const 
//   {
//     if( TMath::Abs(a[0] - b[0])<1.e-4 )
//       {
// 	if( TMath::Abs(a[1] - b[1])<1.e-4 )
// 	  {
// 	    if( TMath::Abs(a[2] - b[2])<1.e-5)
// 	      {
// 		if (TMath::Abs( a[3] - b[3]) < 1.e-3)
// 		  {
// 		    if( TMath::Abs(a[4] - b[4]) < 1.e-3)
// 		      return false;
// 		    else
// 		      return a[4] < b[4];
// 		  }
// 		else
// 		  return a[3] < b[3];
// 	      }
// 	    else
// 	      return a[2] < b[2];
// 	  }
// 	else 
// 	  return a[1] < b[1];
//       }
//     else
//       return a[0] < b[0];
//   }
// };

// ---------------------------------------------------------
class LifetimeModel : public BCModel
{
public:

  // Constructors and destructor
  LifetimeModel(const RooDataSet& dIn, RooRealVar& PoI);
  LifetimeModel(const char * name,const RooDataSet& dIn, RooRealVar& PoI);
  // LifetimeModel(Lambda_LTFit* data1, H3L_LTFit* data2);
  // LifetimeModel(const char* name, Lambda_LTFit* data1, H3L_LTFit* data2);
  LifetimeModel(const TString& namefile);//Lambda_LTFit* data1, H3L_LTFit* data2);
  LifetimeModel(const char* name, const TString& namefile);//Lambda_LTFit* data1, H3L_LTFit* data2);
  ~LifetimeModel();

  // Methods to overload, see file LifetimeModel.cxx
  void DefineParameters();
  //double LogAPrioriProbability(const std::vector<double> &parameters);
  double LogLikelihood(const std::vector<double> &parameters);
  // void MCMCIterationInterface();

private :
  const bool isExpOrSim ; // false = Sim, true = Exp
  const RooDataSet& dataSim;
  RooRealVar& poiSim;

  const TString& nameFile; 
  std::vector<std::unique_ptr<Lambda_LTFit> > data_Lambda;
  std::vector<std::unique_ptr<H3L_LTFit> > data_H3L;

  //std::unordered_map<std::vector<double>, double, HashVec> cache_res;

};
// ---------------------------------------------------------

#endif

