// ***************************************************************
// This file was created using the CreateProject.sh script
// for project Lifetime_Phase0.5.
// CreateProject.sh is part of Bayesian Analysis Toolkit (BAT).
// BAT can be downloaded from http://www.mppmu.mpg.de/bat
// ***************************************************************

#include <BCH1D.h>
#include <BCH2D.h>
#include <BCLog.h>
#include <BCAux.h>
#include <BCSummaryTool.h>

#include "LifetimeModel.h"

#include "TFile.h"
#include "TH1D.h"
#include "TH2F.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TString.h"
#include "TMath.h"

#include "Riostream.h"
#include "TStopwatch.h"

#include "RooWorkspace.h"
#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooPlot.h"
#include "RooFitResult.h"
#include "RooGExpModel.h"
#include "RooExponential.h"
#include "RooFormulaVar.h"
#include "RooMinuit.h"

#include "DataFactory.h"

int main(int argc,char** argv)
{

  if (argc != 2 && argc!=3)
    std::exit(-1);

  TStopwatch timer;
  timer.Start(kFALSE);

  TString SimOrExp(argv[1]);
  TString AdditionalName = argc==3 ? argv[2] : "" ;
  
  // set nicer style for drawing than the ROOT default
  BCAux::SetStyle();
  
  // open log file
  BCLog::OpenLog("log.txt");
  BCLog::SetLogLevel(BCLog::detail);


  LifetimeModel* m = 0;

  if(SimOrExp == "Sim")
    {
      RooWorkspace* w = new RooWorkspace("w",true);
      w->factory("EXPR::model('1/(tau_lt)*TMath::Exp(-l/(tau_lt))',l[0,100],tau_lt[5.48823,3.,14.])");
      RooAbsPdf* model = w->pdf("model");  // pdf*priorNuisance
      model->forceNumInt(kTRUE);
      
      w->defineSet("poi","tau_lt");
      w->defineSet("obs","l");
      
      RooDataSet* dataSim = model->generate(*w->var("l"),2000);

      // create new LifetimeModel object
      m = new LifetimeModel("ModelForSim",*dataSim,*w->var("l"));
      m->SetPriorConstant(0);
    }
  else
    {
      TString nameInFile("./Input/hhh_GenTree_out_Normal_Reco_TOFP_resclsdc_reso1wirebdc_05consisbdc_bdclay56_3000tracksize_1pairsdc_wocutinKalclwdt_barid6495bugfixed_NewTofpKillCondTofpCl_Take_allZ1_allZ2_allpi_firstmap_nwpibar_nwpidpr_1bestpion_New_lifetime_wacc_withbdcbugfixed.root");

      // Lambda_LTFit* LambdaData = new Lambda_LTFit;
      // H3L_LTFit* H3LData = new H3L_LTFit;
      // double z_min = 150.; double z_max = 300.;
      // double tau_min = 0.222 ; double tau_max = 0.42;


      // LambdaData->Init(nameInFile);
      // //LambdaData->SetConditionsCut(z_min,z_max,tau_min,tau_max);
      // cout<<" Lambda Data Loaded !"<<endl;
      // //cout<<" "<<LambdaData->data_t1_6_exp.get()<<endl;
      
      // H3LData->Init(nameInFile);
      // //H3LData->SetConditionsCut(z_min,z_max,tau_min,tau_max);
      // cout<<" H3L Data Loaded !"<<endl;
      

      m = new LifetimeModel("ModelForExp",nameInFile);//LambdaData,H3LData);
      m->SetPriorGauss(0,0.263,0.01);
      //m->SetPriorDelta(0,0.263);
      m->SetPriorConstant(1);
      //m->SetPriorGauss(2,-0.32,0.1);
      m->SetPriorConstant(2);
      m->SetPriorConstant(3);
      // m->SetPriorConstant(4);
      // m->SetPriorConstant(5);
      m->SetPriorConstant(4);
      //m->SetPriorConstant(5);
      // m->SetPriorDelta(3,0.2169);
      // m->SetPriorDelta(4,0.3735);
    }
  
  // set precision
  m->MCMCSetPrecision(BCEngineMCMC::kMedium);
  m->MCMCSetNIterationsRun(10000);

  BCLog::OutSummary("Test model created");

  // create a new summary tool object
  BCSummaryTool * summary = new BCSummaryTool(m);

  m->MCMCSetFlagOrderParameters(true);

  // perform your analysis here

  // normalize the posterior, i.e. integrate posterior
  // over the full parameter space
  //   m->SetIntegrationMethod(BCIntegrate::kDefault);
  //   m->SetIntegrationMethod(BCIntegrate::kIntMonteCarlo);
  //   m->SetIntegrationMethod(BCIntegrate::kIntCuba);
  //   m->SetIntegrationMethod(BCIntegrate::kIntGrid);
  //m->Normalize();

  // run MCMC and marginalize posterior wrt. all parameters
  // and all combinations of two parameters
  //   m->SetMarginalizationMethod(BCIntegrate::kMargDefault);
  m->SetMarginalizationMethod(BCIntegrate::kMargMetropolis);
  //   m->SetMarginalizationMethod(BCIntegrate::kMargMonteCarlo);
  //   m->SetMarginalizationMethod(BCIntegrate::kMargGrid);
  m->MarginalizeAll();

  // run mode finding; by default using Minuit
  //   m->SetOptimizationMethod(BCIntegrate::kOptDefault);
  //   m->SetOptimizationMethod(BCIntegrate::kOptMinuit);
  //   m->SetOptimizationMethod(BCIntegrate::kOptSimAnn);
  //   m->SetOptimizationMethod(BCIntegrate::kOptMetropolis);
  //   m->FindMode();

  // m->SetOptimizationMethod(BCIntegrate::kOptSimAnn);
  // m->SetSASchedule(BCIntegrate::kSACauchy);//kSABoltzmann);
  // m->SetSAT0(1000);
  // m->FindMode(m->GetBestFitParameters());


  // if MCMC was run before (MarginalizeAll()) it is
  // possible to use the mode found by MCMC as
  // starting point of Minuit minimization
  //   m->FindMode( m->GetBestFitParameters() );

  // draw all marginalized distributions into a PostScript file
  //m->PrintAllMarginalized("LifetimeModel_plots.pdf");


  BCLog::OutSummary("Print/Save Marginalisation");
  TString NamePlots("LifetimeModel_");
  TString AllMarg("plots.pdf");
  TString AllMargRoot("plots.root");
  TString nameBase(SimOrExp);
  TString Name1(NamePlots),Name1root(NamePlots);
  Name1+=nameBase;
  Name1+=AdditionalName;
  Name1+=AllMarg;
  Name1root+=nameBase;
  Name1root+=AdditionalName;
  Name1root+=AllMargRoot;
  m->PrintAllMarginalized(Name1.Data());//,"BTsiB3CS1D1smooth1pdf0Lmean","BTcB3CS1meangmode");
  m->SaveAllMarginalized(Name1root.Data());//,"BTsiB3CS1D1smooth1pdf0Lmean","BTcB3CS1meangmode");
  
  BCLog::OutSummary("Print UpdateKnowledge plot");
  TString KnUpdate("_update.pdf");
  TString KnUpdateRoot("_update.root");
  TString Name2(NamePlots),Name2root(NamePlots);
  Name2+=nameBase;
  Name2+=AdditionalName;
  Name2+=KnUpdate;
  Name2root+=nameBase;
  Name2root+=AdditionalName;
  Name2root+=KnUpdateRoot;
  //summary->PrintKnowledgeUpdatePlots(Name2.Data());
  summary->SaveKnowledgeUpdatePlots(Name2root.Data());


  // print individual histograms
  //   m->GetMarginalized("x")->Print("x.pdf");
  //   m->GetMarginalized("y")->Print("y.pdf");
  //   m->GetMarginalized("x", "y")->Print("xy.pdf");

  // print all summary plots
  //   summary->PrintParameterPlot("LifetimeModel_parameters.pdf");
  //   summary->PrintCorrelationPlot("LifetimeModel_correlation.pdf");
  //   summary->PrintKnowledgeUpdatePlots("LifetimeModel_update.pdf");

  // calculate p-value
  //   m->CalculatePValue( m->GetBestFitParameters() );

  // print results of the analysis into a text file
  TString ResultName("_results.txt");
  TString Name3(NamePlots);
  Name3+=nameBase;
  Name3+=AdditionalName;
  Name3+=ResultName;
  m->PrintResults(Name3.Data());

  delete m;
  delete summary;

  BCLog::OutSummary("Test program ran successfully");
  BCLog::OutSummary("Exiting");

  // close log file
  BCLog::CloseLog();

  timer.Stop();
  //delete AnaOutTree;
  

  Double_t rtime = timer.RealTime();
  Double_t ctime = timer.CpuTime();
  std::cout<<"RealTime="<<rtime<<" seconds, CpuTime="<<ctime<<" seconds"<<std::endl;


  return 0;

}

