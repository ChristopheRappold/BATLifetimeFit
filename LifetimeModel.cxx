// ***************************************************************
// This file was created using the CreateProject.sh script.
// CreateProject.sh is part of Bayesian Analysis Toolkit (BAT).
// BAT can be downloaded from http://www.mppmu.mpg.de/bat
// ***************************************************************

#include <omp.h>
#include "LifetimeModel.h"

#include <BCMath.h>
#include "TMath.h"
// ---------------------------------------------------------
LifetimeModel::LifetimeModel(const RooDataSet& dataIn, RooRealVar& PoI) : BCModel(),isExpOrSim(false),dataSim(dataIn),poiSim(PoI),nameFile("")//,data_Lambda(new Lambda_LTFit),data_H3L(new H3L_LTFit)
{
  // default constructor
  DefineParameters();
}

// ---------------------------------------------------------
LifetimeModel::LifetimeModel(const char * name, const RooDataSet& dataIn, RooRealVar& PoI) : BCModel(name),isExpOrSim(false),dataSim(dataIn),poiSim(PoI),nameFile("")//,data_Lambda(new Lambda_LTFit),data_H3L(new H3L_LTFit)
{
  // constructor
  DefineParameters();
}

LifetimeModel::LifetimeModel(/*Lambda_LTFit* data1, H3L_LTFit* data2*/const TString& nameF) : BCModel(),isExpOrSim(true),dataSim(*(new RooDataSet)),poiSim(*(new RooRealVar)),nameFile(nameF)//,data_Lambda(data1),data_H3L(data2)
{
  int nth = 4;//omp_get_num_threads();
  cout<<" Nb threads :"<<nth<<endl;
  
data_Lambda.resize(nth);
  for(int i=0;i<nth;++i)
    {
      TString nameID("");
      nameID+=i;
      data_Lambda[i] = std::move(std::unique_ptr<Lambda_LTFit>(new Lambda_LTFit(nameID,i)));
      data_Lambda[i]->Init(nameFile);
    }

  data_H3L.resize(nth);
  for(int i=0;i<nth;++i)
    {
      TString nameID("");
      nameID+=i;
      data_H3L[i] = std::move(std::unique_ptr<H3L_LTFit>(new H3L_LTFit(nameID,i)));
      data_H3L[i]->Init(nameFile);
    }


  // default constructor
  DefineParameters();
}

// ---------------------------------------------------------
LifetimeModel::LifetimeModel(const char * name, /*Lambda_LTFit* data1, H3L_LTFit* data2*/const TString& nameF) : BCModel(name),isExpOrSim(true),dataSim(*(new RooDataSet)),poiSim(*(new RooRealVar)),nameFile(nameF)//,data_Lambda(data1),data_H3L(data2)
{

  int nth = 4;//omp_get_num_threads();
  cout<<" Nb threads :"<<nth<<endl;

  data_Lambda.resize(nth);
  for(int i=0;i<nth;++i)
    {
      TString nameID("");
      nameID+=i;
      data_Lambda[i] = std::move(std::unique_ptr<Lambda_LTFit>(new Lambda_LTFit(nameID,i)));
      data_Lambda[i]->Init(nameFile);
    }

  data_H3L.resize(nth);
  for(int i=0;i<nth;++i)
    {
      TString nameID("");
      nameID+=i;
      data_H3L[i] = std::move(std::unique_ptr<H3L_LTFit>(new H3L_LTFit(nameID,i)));
      data_H3L[i]->Init(nameFile);
    }

  for(int i=0;i<nth;++i)
    {
      cout<<"Data #"<<i<<endl;
      data_Lambda.at(i)->Print();
      data_H3L.at(i)->Print();
    }


  // constructor
  DefineParameters();
}



// ---------------------------------------------------------
LifetimeModel::~LifetimeModel()
// default destructor
{
}

// ---------------------------------------------------------
void LifetimeModel::DefineParameters()
{
  // Add parameters to your model here.
  // You can then use them in the methods below by calling the
  // parameters.at(i) or parameters[i], where i is the index
  // of the parameter. The indices increase from 0 according to the
  // order of adding the parameters.

  AddParameter("tauLambda", 0.200,0.300); // index 0
  if(isExpOrSim)
    {
      AddParameter("tauH3L", 0.010,0.500); // index 1
      AddParameter("EffCorrection",0.0001,0.1); // -2., -0.1
      //AddParameter("EffCorrectionNi",0.1,5); // -2., -0.1
      // AddParameter("EffCorrectionRho",0.1,1); // -2., -0.1
      // AddParameter("EffCorrectionDelta",0.1,1); // -2., -0.1
      AddParameter("TauMin",0.211,0.24); // 0.211, 0.3
      AddParameter("TauMax",0.32,0.42); // 0.3, 0.442
    }
}

// ---------------------------------------------------------
double LifetimeModel::LogLikelihood(const std::vector<double> &parameters)
{
  // This methods returns the logarithm of the conditional probability
  // p(data|parameters). This is where you have to define your model.

  double logprob = 0.;

  // if(isExpOrSim==false)
  //   {
  //     double tau = parameters.at(0);
  //     //   double y = parameters.at(1);
  //     //   double eps = 0.5;
       
  //     // Breit-Wigner distribution of x with nuisance parameter y
  //     //   logprob += BCMath::LogBreitWignerNonRel(x + eps*y, 0.0, 1.0);
  //     double sumXi = dataSim.mean(poiSim)*dataSim.numEntries();
  //     double ni = dataSim.numEntries();
       
  //     logprob = -ni * TMath::Log(tau) - 1./tau * sumXi;
  //     return logprob;
  //   }
  // else
  //   {
      // bool found=false;
      // double res_logL = 0;

// #pragma omp critical
//       {
// 	std::map<std::vector<double> , double, CompVec>::const_iterator it_cache = cache_res.find(parameters);
// 	if(it_cache != cache_res.end())
// 	  {
// 	    found = true;
// 	    res_logL = it_cache->second; 
// 	  }
//       }
 
//       if(found)
// 	return res_logL;
      
  //cout<<" thread :"<<omp_get_thread_num()<<" / "<<omp_get_num_threads()<<" : "<<omp_get_num_procs()<<" "<<omp_get_active_level()<<endl;
      
      int index = omp_get_thread_num();
      //printf("Thread rank: %d\n", omp_get_thread_num());
      double LogL_Lambda = data_Lambda.at(index)->CalculateLogLikelihood(parameters);
      double LogL_H3L = data_H3L.at(index)->CalculateLogLikelihood(parameters);

      // for(int i=0;i<data_Lambda.size();++i)
      // 	{
      // 	  cout<<"Data #"<<i<<endl;
      // 	  data_Lambda.at(i)->Print();
      // 	  data_H3L.at(i)->Print();
      // 	}


      logprob = LogL_H3L + LogL_Lambda;

// #pragma omp flush (cache_res)
// #pragma omp atomic
//       cache_res.insert(std::pair<std::vector<double>, double>(parameters,logprob));
      // log L (Lambda) = sysNS*(-Sum(wi)*( log(tauLambda) - log( exp(-tauMin/tauLambda) -exp(-tauMax/tauLambda) ) ) - Sum(wi*ti)/(tauLambda) ) + MaxLogL
      // log L (H3L) = sysNS*(-Sum(wi)*( log(tauH3L) - log( exp(-tauMin/tauH3L) -exp(-tauMax/tauH3L) ) ) - Sum(wi*ti)/(tauH3L) ) + MaxLogL
      // wi : wi * / Sum wi * N 

      return logprob;
      // }
}

// // ---------------------------------------------------------
// double LifetimeModel::LogAPrioriProbability(const std::vector<double> &parameters)
// {
//    // This method returns the logarithm of the prior probability for the
//    // parameters p(parameters).

//    double logprob = 0.;

// //   double x = parameters.at(0);
// //   double y = parameters.at(1);

// //   double dx = GetParameter(0)->GetRangeWidth();

// //   logprob += log(1./dx);                    // flat prior for x
// //   logprob += BCMath::LogGaus(y, 0., 1.0);   // Gaussian prior for y

//    return logprob;
// }
// ---------------------------------------------------------

