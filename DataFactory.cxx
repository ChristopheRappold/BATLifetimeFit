#include "DataFactory.h"

using namespace RooFit ;

Lambda_LTFit::Lambda_LTFit(const TString& i, int id):nameID(i),ID(id)
{
  decayT = new RooRealVar("LaDecayTime"+nameID,"LaDecayTime"+nameID,0,1);
  Sc = new RooRealVar("LaSc"+nameID,"LaSc"+nameID,-200,200);
  W = new RooRealVar("LaW"+nameID,"LaW"+nameID,-1e7,1e7) ;
  VtxZ = new RooRealVar("LaVtxZ"+nameID,"LaVtxZ"+nameID,-100,400);
  DeltaL = new RooRealVar("LaDeltaL"+nameID,"LaDeltaL"+nameID,0,40);
  Mass = new RooRealVar("LaInvMass"+nameID,"LaInvMass"+nameID,1,5) ; 
  Factor = new RooRealVar("LaFactor"+nameID,"LaFactor"+nameID,0,10);


  h0 = new RooDataSet("LadataSB_"+nameID,"LadataSB_"+nameID,RooArgSet(*decayT,*Sc,*W,*VtxZ,*DeltaL,*Factor)) ;
  b1 = new RooDataSet("LadataB1_"+nameID,"LadataB1_"+nameID,RooArgSet(*decayT,*Sc,*W,*VtxZ,*DeltaL,*Factor)) ;
  b2 = new RooDataSet("LadataB2_"+nameID,"LadataB2_"+nameID,RooArgSet(*decayT,*Sc,*W,*VtxZ,*DeltaL,*Factor)) ;


    
  // LogL = new TF1("LambdaLogL", "[0]*([1]*(log(1./(x-[4])) - log( exp(-[5]/x) -exp(-[6]/x) )) - [2]/(x-[4])) + [3]",0,1);
  // LogL->SetNpx(1000);
  // rand = new TRandom3(1);
}
  
#ifdef HIDE_OLD

void Lambda_LTFit::processFit (double z_min,double z_max, double tau_minCut, double tau_maxCut, std::vector<double>& fittedValues, double NewCalibCorrSys)
{ 
  bool newCalib = TMath::Abs(NewCalibCorrSys-1.)>1e-8 ? true : false;
  
  // cout<<"--- ProcessFit "<<endl;
  // cout<<" newCalib ?"<<newCalib<<endl;

  TString temp_cut("LaVtxZ>= "),temp_cut2(" && LaVtxZ<= "),temp_cut3(" && LaDecayTime >= "), temp_cut4(" && LaDecayTime <= ");
  temp_cut+=z_min;
  temp_cut+=temp_cut2;
  temp_cut+=z_max;
  
  temp_cut+=temp_cut3;
  temp_cut+=tau_minCut;//tau_minCut;
  temp_cut+=temp_cut4;
  temp_cut+=tau_maxCut;//tau_maxCut;


  
  //std::auto_ptr<RooDataSet> data_t1_6_exp (dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cut)))) ;
  std::unique_ptr<RooDataSet> data_t1_6_exp (dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cut)))) ;
  //data_t1_6_exp = (dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cut)))) ;
  RooRealVar* newW_obsLa = 0;
  RooRealVar* LW_obsLa = 0;
  if(newCalib)
    {
      RooRealVar New_CorrLa("New_CorrLa","New_CorrLa",NewCalibCorrSys);
      //cout<<" New Corr "<<LOldCorr/LCorrNewFactor<<endl;
      RooFormulaVar New_WLa("New_WLa","LaW*pow(LaFactor,New_CorrLa-1.)",RooArgSet(*W,*Factor,New_CorrLa));
      newW_obsLa = dynamic_cast<RooRealVar*>( data_t1_6_exp->addColumn(New_WLa) );
      
      RooFormulaVar LWLa("LWLa","LaDecayTime*New_WLa",RooArgSet(*decayT,*newW_obsLa)) ;
      //RooRealVar* NW_obs = (RooRealVar*) temp_h0->addColumn(NW);
      LW_obsLa = dynamic_cast<RooRealVar*>( data_t1_6_exp->addColumn(LWLa) ) ;
    }

  //double meanL = data_t1_6_exp->mean(decayT);
  
  //data_t1_6_exp->Print("v");
  
  double meanW = newCalib ? data_t1_6_exp->mean(*newW_obsLa) : data_t1_6_exp->mean(*W);
  double meanLW = newCalib ? data_t1_6_exp->mean(*LW_obsLa) : data_t1_6_exp->mean(*LW_obs);

  double  sysNS = data_t1_6_exp->mean(*Sc)/meanW;

  LogL->SetParameter(0,data_t1_6_exp->numEntries()*sysNS);
  LogL->SetParameter(1,meanW);
  LogL->SetParameter(2,meanLW);
  LogL->SetParameter(3,0.);
  LogL->SetParameter(4,0.);
  LogL->SetParameter(5,tau_minCut);
  LogL->SetParameter(6,tau_maxCut);

  double sysOffset = -LogL->GetMaximum(0.001,1);
  LogL->SetParameter(3,sysOffset);
  
  double sys_min = LogL->GetMaximumX(0.001,1.);
  
  double sys_onesigma_low = LogL->GetX(-0.5,0.001,sys_min-0.000001);
  double sys_onesigma_high = LogL->GetX(-0.5,sys_min+0.000001,1);

  fittedValues.resize(3);
  fittedValues[0]=(sys_min);
  fittedValues[1]=(sys_min-sys_onesigma_low);
  fittedValues[2]=(sys_onesigma_high-sys_min);


}

int Lambda_LTFit::processFactor (double z_min,double z_max, double tau_minCut, double tau_maxCut, std::vector<double>& resValues)
{ 
  
  double ref_LT = 0.263;

  TString temp_cut("LaVtxZ>= "),temp_cut2(" && LaVtxZ<= "),temp_cut3(" && LaDecayTime >= "), temp_cut4(" && LaDecayTime <= ");
  temp_cut+=z_min;
  temp_cut+=temp_cut2;
  temp_cut+=z_max;
  
  temp_cut+=temp_cut3;
  temp_cut+=tau_minCut;//tau_minCut;
  temp_cut+=temp_cut4;
  temp_cut+=tau_maxCut;//tau_maxCut;

  std::vector<double> resFit_noFactor (3);
  processFit(z_min,z_max,tau_minCut,tau_maxCut,resFit_noFactor,0.001);
  bool testMin = false;
  if(ref_LT<resFit_noFactor[0])
    {
      cout<<"E> LT without factor should be lower than 0.263 ns "<<resFit_noFactor[0]<<endl;
      
      processFit(z_min,z_max,tau_minCut,tau_maxCut,resFit_noFactor,1e-9);
      if(ref_LT<resFit_noFactor[0])
	return -2;
      else 
	testMin = true;
      //assert(ref_LT>resFit_noFactor[0]);
    }
  
  std::vector<double> resFit_TreeFactor (3);
  processFit(z_min,z_max,tau_minCut,tau_maxCut,resFit_TreeFactor,1.);

  double tempFactor = 0.;
  std::vector<double> previousFactor(2);

  if(resFit_TreeFactor[0]>ref_LT)
    {
      tempFactor = rand->Uniform(0.001,1);
      previousFactor[0] = testMin ? 1e-9 : 0.001;
      previousFactor[1] = 1;
    }
  else
    {
      tempFactor = rand->Uniform(1,10);
      previousFactor[0] = 1;
      previousFactor[1] = 10;
    }

  std::vector<double> resFit_TempFactor (3);
  
  bool toContinue = true;
  double diff_LT = 100000;
  int nIter = 0;

  while(toContinue)
    {
      processFit(z_min,z_max,tau_minCut,tau_maxCut,resFit_TempFactor,tempFactor);

      diff_LT = resFit_TempFactor[0] - ref_LT;
      if(TMath::Abs(diff_LT)<1e-4)
	toContinue = false;
      else
	{
	  if(diff_LT>0.)
	    {
	      previousFactor[1] = tempFactor;
	      tempFactor = rand->Uniform(previousFactor[0],tempFactor);
	    }
	  else if(diff_LT<0.)
	    {
	      previousFactor[0] = tempFactor;
	      tempFactor = rand->Uniform(tempFactor,previousFactor[1]);
	    }
	}
      ++nIter;
      if(nIter>200)
	return -1;
    }


  resValues.resize(4);
  resValues[0] = tempFactor;
  resValues[1] = resFit_TempFactor[0];
  resValues[2] = resFit_TempFactor[1];
  resValues[3] = resFit_TempFactor[2];
  return 0;
}
#endif

Lambda_LTFit::Lambda_LTFit(const Lambda_LTFit& data):nameID(data.nameID),ID(data.ID)
{
  decayT = new RooRealVar("LaDecayTime"+nameID,"LaDecayTime"+nameID,0,1);
  Sc = new RooRealVar("LaSc"+nameID,"LaSc"+nameID,-200,200);
  W = new RooRealVar("LaW"+nameID,"LaW"+nameID,-1e7,1e7) ;
  VtxZ = new RooRealVar("LaVtxZ"+nameID,"LaVtxZ"+nameID,-100,400);
  DeltaL = new RooRealVar("LaDeltaL"+nameID,"LaDeltaL"+nameID,0,40);
  Mass = new RooRealVar("LaInvMass"+nameID,"LaInvMass"+nameID,1,5) ; 
  Factor = new RooRealVar("LaFactor"+nameID,"LaFactor"+nameID,0,10);

  TString new_name[3] ={data.h0->GetName(),data.b1->GetName(),data.b2->GetName()};
  new_name[0]+="Copy";
  new_name[1]+="Copy";
  new_name[2]+="Copy";
  new_name[0]+=nameID;
  new_name[1]+=nameID;
  new_name[2]+=nameID;

  h0 = new RooDataSet(*data.h0,new_name[0]);
  b1 = new RooDataSet(*data.b1,new_name[1]);
  b2 = new RooDataSet(*data.b2,new_name[2]);

  Zmin = data.Zmin;
  Zmax = data.Zmax;
  TauMinCut = data.TauMinCut;
  TauMaxCut = data.TauMaxCut;

}

void Lambda_LTFit::Print() const
{
  cout<<"-- Lambda_LTFit class :"<<endl;
  cout<<"data pointer :"<<h0<<" "<<b1<<" "<<b2<<endl;
  h0->Print();
  //cout<<"reduced data pointer :"<<data_t1_6_exp.get()<<endl;
  
}

void Lambda_LTFit::Init(const TString& namefile)
{
  
  double peak_low = 0;//peakposition-peakwidth/2.0;
  double peak_high = 0;//peakposition+peakwidth/2.0;

  double bg1_low = 0;//peak_low - distancetobg - bgwidth;
  double bg1_high =0;//peak_low - distancetobg;

  double bg2_low = 0;//peak_high + distancetobg ;
  double bg2_high = 0;//peak_high + distancetobg + bgwidth;

  //peak_low = 1.104;
  //peak_high = 1.118;
  peak_low = 1.1023;
  peak_high = 1.1190;
  
  /*
    bg1_low = peak_low-0.5*(peak_high-peak_low);
    bg1_high = peak_low;
  
    bg2_low = peak_high;
    bg2_high = peak_high+0.5*(peak_high-peak_low);
  */
  
  bg1_low = 1.0+0.01;
  bg2_high =1.5-0.01;

  bg1_high = peak_low;
  bg2_low = peak_high;
  


  //TFile* fmixed = new TFile("hhh_Data_reco_tofs9_pvavpr099_mix_binning.root");
  //TFile* fmixed = new TFile("~/sandbox/ReadRecTree_BestInvestigation_normcomb/hhh_Data_reco_tofs9_pvavpr099_mix_finbinning_nw.root");
  //TFile* fmixed = new TFile("~/sandbox/ReadRecTree_BestInvestigation_normcomb/hhh_Data_lifecondition_mix1_and11_finbinning.root");
  //TFile* fmixed = new TFile("~/sandbox/ReadRecTree_BestInvestigation_normcomb/hhh_out_Mixed_Reco_TOFP_resclsdc_reso1wirebdc_05consisbdc_bdclay56_3000tracksize_1pairsdc_wocutinKalclwdt_barid6495bugfixed_NewTofpKillCondTofpCl_Take_allZ1_allZ2_allpi_firstmap_nwpibar_nwpidpr_1bestpion_1enmix11enmix_lifecond_finebinning.root");
  TFile* fmixed = new TFile("./Input/hhh_out_Mixed_Reco_TOFP_resclsdc_reso1wirebdc_05consisbdc_bdclay56_3000tracksize_1pairsdc_wocutinKalclwdt_barid6495bugfixed_NewTofpKillCondTofpCl_Take_allZ1_allZ2_allpi_firstmap_nwpibar_nwpidpr_1bestpion_1enmix11enmix_lifecond_finebinning.root");
  TH1F* hmixed = 0;

  hmixed = (TH1F*)fmixed->Get("mother0_all_mix");

  int bin_peak_low = hmixed->GetXaxis()->FindBin(peak_low);
  int bin_peak_high = hmixed->GetXaxis()->FindBin(peak_high);
  int bin_bg1_low = hmixed->GetXaxis()->FindBin(bg1_low);
  int bin_bg1_high = hmixed->GetXaxis()->FindBin(bg1_high);
  int bin_bg2_low = hmixed->GetXaxis()->FindBin(bg2_low);
  int bin_bg2_high = hmixed->GetXaxis()->FindBin(bg2_high);

  double integral_peak = hmixed->Integral(bin_peak_low,bin_peak_high);
  double integral_bg1 = hmixed->Integral(bin_bg1_low,bin_bg1_high);
  double integral_bg2 = hmixed->Integral(bin_bg2_low,bin_bg2_high);

  double norm_bg1 = integral_bg1/(0.5*integral_peak);
  double norm_bg2 = integral_bg2/(0.5*integral_peak);

  //norm_bg1 = norm_bg1*1.15; // 1.1 phase 0
  //norm_bg2 = norm_bg2*1.15; // 1.1
  norm_bg1 = norm_bg1*1.0; // 
  norm_bg2 = norm_bg2*1.0; // 
  

  cout << integral_bg1/(0.5*integral_peak) << " " << integral_bg2/(0.5*integral_peak) << endl;
  cout << 1/norm_bg1 << " " << 1/norm_bg2 << endl;

  TFile* fin = 0;
  
  fin  = TFile::Open(namefile);

  fin->cd();
  TTree* tv__tree = (TTree *) fin->Get("Data_tree");

  Long64_t total_nentries= tv__tree->GetEntries();
  cout<<"N event :"<<total_nentries<<endl;
  Long64_t timing=0; 
  
  Float_t In_InvMass, In_vtx_X,In_vtx_Y,In_vtx_Z,In_DLbetgam,In_tau,In_Eff,In_Ncand,In_Lfactor,In_LExpCorr;
  Int_t In_type,In_SB;
  Int_t In_Nevent;

  tv__tree->SetBranchAddress("InvMass",&In_InvMass);
  tv__tree->SetBranchAddress("vtx_X",&In_vtx_X);
  tv__tree->SetBranchAddress("vtx_Y",&In_vtx_Y);
  tv__tree->SetBranchAddress("vtx_Z",&In_vtx_Z);
  tv__tree->SetBranchAddress("DL_betagamma",&In_DLbetgam);
  tv__tree->SetBranchAddress("Eff",&In_Eff);
  tv__tree->SetBranchAddress("tau",&In_tau);
  tv__tree->SetBranchAddress("Ncand",&In_Ncand);
  tv__tree->SetBranchAddress("Lfactor",&In_Lfactor);
  tv__tree->SetBranchAddress("LExpCorr",&In_LExpCorr);

  tv__tree->SetBranchAddress("Nevent",&In_Nevent);
  tv__tree->SetBranchAddress("type",&In_type);
  tv__tree->SetBranchAddress("SB",&In_SB);


  // RooRealVar decayT("DecayTime","DecayTime",0,1) ;
  // RooRealVar Sc("Sc","Sc",-200,200) ;
  // RooRealVar W("W","W",-1e7,1e7) ;
  // RooRealVar VtxZ("VtxZ","VtxZ",-100,400) ;
  // RooRealVar DeltaL("DeltaL","DeltaL",0,40) ;
  // RooRealVar Mass("InvMass","InvMass",1,5) ;
  
  // RooDataSet h0("dataSB","dataSB",RooArgSet(decayT,Sc,W,VtxZ,DeltaL)) ;
  // RooDataSet b1("dataB1","dataB1",RooArgSet(decayT,Sc,W,VtxZ,DeltaL)) ;
  // RooDataSet b2("dataB2","dataB2",RooArgSet(decayT,Sc,W,VtxZ,DeltaL)) ;
 

  for(Long64_t i = 0;i<total_nentries;++i)
    {
      tv__tree->GetEntry(i);
      if(i%100000==0)
	std::cout<<"Processing Lambda Event#"<<i<<" | "<<(double)i/(double)(total_nentries)*100<<" %"<<std::endl;
      
      if((int)((double)i/(double)(total_nentries)*10)==timing)
	{
	  std::cout<<"Progress Lambda:"<<(int)((double)i/(double)(total_nentries)*100.)<<" %"<<std::endl;
	  ++timing; 
	}
      
      bool filling_condition = false;

      if(In_type==0)
	filling_condition = true;


      if(filling_condition )
	{
	  //h_InvMass->Fill(temp_m.M());
	  
	  double temp_lt = In_tau;
	  *decayT = temp_lt;
	  *VtxZ = In_vtx_Z;
	    
	  double temp_delta = 0;
	    
	  double func_eff = 1./In_Eff;
	    
	  if(In_SB >=9)
	    {
	      *W = 1./func_eff*In_Ncand;
	      *Sc = In_Ncand;
	      *DeltaL = temp_delta;
	      *Factor = In_Lfactor;

	      h0->add(RooArgSet(*decayT,*Sc,*W,*VtxZ,*DeltaL,*Factor));
	    }
	      
	  if(In_SB==0)
	    {
	      *W = -1./func_eff/norm_bg1*In_Ncand;
	      *Sc = -1./norm_bg1*In_Ncand;
	      *DeltaL = temp_delta;
	      *Factor = In_Lfactor;

	      b1->add(RooArgSet(*decayT,*Sc,*W,*VtxZ,*DeltaL,*Factor));
	    }
	  if(In_SB==5)
	    {
	      *W = -1./func_eff/norm_bg2*In_Ncand;
	      *Sc = -1./norm_bg2*In_Ncand;
	      *DeltaL = temp_delta;
	      *Factor = In_Lfactor;

	      b2->add(RooArgSet(*decayT,*Sc,*W,*VtxZ,*DeltaL,*Factor));
	    }
	}
    }
	
	    
  cout<<" hist entries h0: "<<h0->numEntries()<<endl;//" "<<h0_1.numEntries()<<" "<<h0_2.numEntries()<<endl;
  cout<<" hist entries b1: "<<b1->numEntries()<<endl;//" "<<b1_1.numEntries()<<" "<<b1_2.numEntries()<<endl;
  cout<<" hist entries b2: "<<b2->numEntries()<<endl;//" "<<b2_1.numEntries()<<" "<<b2_2.numEntries()<<endl;

  
  h0->append(*b1);
  h0->append(*b2);


  // RooRealVar NormWeigth("LaNormWeigth"+nameID,"LaNormWeigth"+nameID,h0->mean(*Sc)/h0->mean(*W));
  // RooFormulaVar NW("LaNW"+nameID,"LaNormWeigth"+nameID+"*LaW"+nameID,RooArgSet(NormWeigth,*W)) ;
  // RooFormulaVar LW("LaLW"+nameID,"LaDecayTime"+nameID+"*LaW"+nameID,RooArgSet(*decayT,*W)) ;
  // NW_obs = (RooRealVar*) h0->addColumn(NW);
  // LW_obs = (RooRealVar*) h0->addColumn(LW);

  //cout<< wdata_S_exp->mean(decayT) << " "<< NormWeigth.getVal()<<endl;
  //cout<< h0->mean(*LW_obs)*h0->numEntries()<< " "<<h0->mean(*decayT)*h0->numEntries() <<" "<<h0->mean(*W) *h0->numEntries()  <<" "<< NormWeigth.getVal()<<endl;



}

void Lambda_LTFit::SetConditionsCut(double z_min,double z_max, double tau_minCut, double tau_maxCut)
{

  // TString temp_cut("LaVtxZ>= "),temp_cut2(" && LaVtxZ<= "),temp_cut3(" && LaDecayTime >= "), temp_cut4(" && LaDecayTime <= ");
  // temp_cut+=z_min;
  // temp_cut+=temp_cut2;
  // temp_cut+=z_max;
  
  // temp_cut+=temp_cut3;
  // temp_cut+=tau_minCut;//tau_minCut;
  // temp_cut+=temp_cut4;
  // temp_cut+=tau_maxCut;//tau_maxCut;
  
  //data_t1_6_exp = std::auto_ptr<RooDataSet>(dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cut))));

  // std::unique_ptr<RooDataSet> tempUnique(dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cut))));
  // data_t1_6_exp = std::move(tempUnique);

  //data_t1_6_exp.reset(dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cut))));
  
  //cout<<" CutCondition Lambda :"<<data_t1_6_exp.get()<<endl;
  //data_t1_6_exp = std::unique_ptr<RooDataSet>(dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cut))));
  //data_t1_6_exp = dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cut)));

  Zmin = z_min;
  Zmax = z_max;
  TauMinCut = tau_minCut;
  TauMaxCut = tau_maxCut;



}

double Lambda_LTFit::CalculateLogLikelihood(const std::vector<double>& parameters)
{
  double tauLambda = parameters[0];
  //double tauH3L    = parameters[1];
  double CorrEff   = parameters[2];
  //double CorrNi    = parameters[3];
  // double CorrRho   = parameters[4];
  // double CorrDelt  = parameters[5];
  double TauMin    = parameters[3];
  double TauMax    = parameters[4];
  
  this->SetConditionsCut(150.,300.,TauMin,TauMax);

  std::unordered_map<std::vector<double>, double, HashVec>::const_iterator it_cache = cache_res.find(parameters);
  if(it_cache != cache_res.end())
    return it_cache->second;

  //TString temp_cut("LaVtxZ"+nameID+">= "),temp_cut2(" && LaVtxZ"+nameID+"<= "),temp_cut3(" && LaDecayTime"+nameID+" >= "), temp_cut4(" && LaDecayTime"+nameID+" <= ");

  // TString tempSign[2]={" >= "," <= "};
  // TString temp_cut("LaVtxZ"),temp_cut2(" && LaVtxZ"),temp_cut3(" && LaDecayTime"), temp_cut4(" && LaDecayTime");
  // temp_cut+=ID; temp_cut+=tempSign[0];
  // temp_cut2+=ID; temp_cut2+=tempSign[1];
  // temp_cut3+=ID; temp_cut3+=tempSign[0];
  // temp_cut4+=ID; temp_cut4+=tempSign[1];

  // temp_cut+=Zmin;
  // temp_cut+=temp_cut2;
  // temp_cut+=Zmax;
  
  // temp_cut+=temp_cut3;
  // temp_cut+=TauMinCut;//tau_minCut;
  // temp_cut+=temp_cut4;
  // temp_cut+=TauMaxCut;//tau_maxCut;

  
  
  //h0->Print();
  //cout<<" doing reduce "<<temp_cut<<endl;
  //data_t1_6_exp = std::auto_ptr<RooDataSet>(dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cut))));
  //std::unique_ptr<RooDataSet> tempUnique(dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cut))));
  //RooDataSet* tempUnique = (dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cut))));
  

  // data_Lambda->data_t1_6_exp->Print() ;
  
  //const RooDataSet& ref_LambdaData (*data_Lambda->data_t1_6_exp);
  //cout<<" "<<data_Lambda->data_t1_6_exp.get()<<endl;
  
  const RooArgSet* obsLambda = h0->get() ;
  
  
  
  RooRealVar* DTdata = dynamic_cast<RooRealVar*>(obsLambda->find(this->decayT->GetName())) ;
  RooRealVar* Ndata  = dynamic_cast<RooRealVar*>(obsLambda->find(this->Sc->GetName())) ;
  RooRealVar* Wdata  = dynamic_cast<RooRealVar*>(obsLambda->find(this->W->GetName())) ;
  RooRealVar* vtxZdata  = dynamic_cast<RooRealVar*>(obsLambda->find(this->VtxZ->GetName())) ;
  
  double sum_wi = 0.;
  double sum_witi = 0.;
  double normalizeW = 0.;
  
  for(int i=0 ; i<h0->numEntries();++i)
    {
      h0->get(i);
      double vtx_zi = vtxZdata->getVal(); 
      double ti = DTdata->getVal();
      if(Zmin <= vtx_zi && vtx_zi <= Zmax && TauMinCut <= ti && ti <= TauMaxCut)  
	{ 
	  double wi = Wdata->getVal();
	  double ni = Ndata->getVal();
	  
	  double corr_i = exp(-ti/CorrEff)/(CorrEff*(exp(-TauMinCut/CorrEff)-exp(-TauMaxCut/CorrEff)));
	  //double corr_i = TMath::Exp(-ti/CorrEff-ni/CorrNi);//-CorrRho*TMath::Max(ti,ni+TMath::Min(ti,CorrDelt))); 
	  double new_wi = wi*corr_i;
	  
	  sum_witi += ti*new_wi;
	  sum_wi += new_wi;
	  
	  normalizeW += ni;
	}
    }
  normalizeW /= sum_wi;
  
  double LogL_Lambda = normalizeW*(sum_wi* (-log(tauLambda) - log(exp(-TauMinCut/tauLambda) - exp(-TauMaxCut/tauLambda)) ) - sum_witi/tauLambda);

  cache_res.insert(std::pair<std::vector<double>, double>(parameters,LogL_Lambda));

  return LogL_Lambda;
}


H3L_LTFit::H3L_LTFit(const TString& i, int iD):nameID(i),ID(iD)
{
  decayT = new RooRealVar("H3LDecayTime"+nameID,"H3LDecayTime"+nameID,0,1);
  Sc = new RooRealVar("H3LSc"+nameID,"H3LSc"+nameID,-200,200);
  W = new RooRealVar("H3LW"+nameID,"H3LW"+nameID,-1e7,1e7) ;
  VtxZ = new RooRealVar("H3LVtxZ"+nameID,"H3LVtxZ"+nameID,-100,400);
  DeltaL = new RooRealVar("H3LDeltaL"+nameID,"H3LDeltaL"+nameID,0,40);
  Mass = new RooRealVar("H3LInvMass"+nameID,"H3LInvMass"+nameID,1,5) ; 
  Factor = new RooRealVar("H3LFactor"+nameID,"H3LFactor"+nameID,0,10);


  h0 = new RooDataSet("H3LdataSB_"+nameID,"H3LdataSB_"+nameID,RooArgSet(*decayT,*Sc,*W,*VtxZ,*DeltaL,*Factor)) ;
  b1 = new RooDataSet("H3LdataB1_"+nameID,"H3LdataB1_"+nameID,RooArgSet(*decayT,*Sc,*W,*VtxZ,*DeltaL,*Factor)) ;
  b2 = new RooDataSet("H3LdataB2_"+nameID,"H3LdataB2_"+nameID,RooArgSet(*decayT,*Sc,*W,*VtxZ,*DeltaL,*Factor)) ;

    
  // LogL = new TF1("H3LLogL", "[0]*([1]*(log(1./(x-[4])) - log( exp(-[5]/x) -exp(-[6]/x) )) - [2]/(x-[4])) + [3]",0,1);
  // LogL->SetNpx(1000);
  // rand = new TRandom3(1);
}
  
H3L_LTFit::H3L_LTFit(const H3L_LTFit& data):nameID(data.nameID),ID(data.ID)
{
  decayT = new RooRealVar("H3LDecayTime"+nameID,"H3LDecayTime"+nameID,0,1);
  Sc = new RooRealVar("H3LSc"+nameID,"H3LSc"+nameID,-200,200);
  W = new RooRealVar("H3LW"+nameID,"H3LW"+nameID,-1e7,1e7) ;
  VtxZ = new RooRealVar("H3LVtxZ"+nameID,"H3LVtxZ"+nameID,-100,400);
  DeltaL = new RooRealVar("H3LDeltaL"+nameID,"H3LDeltaL"+nameID,0,40);
  Mass = new RooRealVar("H3LInvMass"+nameID,"H3LInvMass"+nameID,1,5) ; 
  Factor = new RooRealVar("H3LFactor"+nameID,"H3LFactor"+nameID,0,10);

  TString new_name[3] ={data.h0->GetName(),data.b1->GetName(),data.b2->GetName()};
  new_name[0]+="Copy";
  new_name[1]+="Copy";
  new_name[2]+="Copy";
  new_name[0]+=nameID;
  new_name[1]+=nameID;
  new_name[2]+=nameID;

  h0 = new RooDataSet(*data.h0,new_name[0]);
  b1 = new RooDataSet(*data.b1,new_name[1]);
  b2 = new RooDataSet(*data.b2,new_name[2]);

  LOldCorr = LOldCorr;

  Zmin = data.Zmin;
  Zmax = data.Zmax;
  TauMinCut = data.TauMinCut;
  TauMaxCut = data.TauMaxCut;

}

void H3L_LTFit::Print() const
{
  cout<<"-- H3L_LTFit class :"<<endl;
  cout<<"data pointer :"<<h0<<" "<<b1<<" "<<b2<<endl;
  h0->Print();
  //cout<<"reduced data pointer :"<<data_t1_6_exp.get()<<endl;
  
}


void H3L_LTFit::Init(const TString& namefile,int type)
{
  
  double peak_low = 0;//peakposition-peakwidth/2.0;
  double peak_high = 0;//peakposition+peakwidth/2.0;

  double bg1_low = 0;//peak_low - distancetobg - bgwidth;
  double bg1_high =0;//peak_low - distancetobg;

  double bg2_low = 0;//peak_high + distancetobg ;
  double bg2_high = 0;//peak_high + distancetobg + bgwidth;

  if(type==0)  
    {
      //#ifdef Lambda
      peak_low = 1.1023;
      peak_high = 1.1190;
      //peak_low = 1.104;
      //peak_high = 1.118;

      //#endif
    }
  else if(type==1)
    {
      //#ifdef H3L
      //peak_low = 2.980;
      //peak_high = 2.993;
      peak_low = 2.981;
      peak_high = 2.993;
      //#endif
    }
  else if(type==2)
    {
      //#ifdef H4L
      peak_low = 3.90;
      peak_high = 3.91;
      //#endif
    }
  else if (type==3)
    {
      //#ifdef dpi
      std::exit(1);
      //bgwidth = 0.0067*2.;
      //#endif
    }
  else if(type==4)
    {
      //#ifdef tpi
      // peakposition = 2.9807;
      // peakwidth = 0.0064*4.;//*0.5;//*3.;
      // distancetobg = 0.0064*(0);
      // bgwidth = 0.0064*2.;//*0.5;//*1.5;
      std::exit(1);

      //#endif
    }


  if(type==0)
    {
      bg1_low = 1.0+0.01;
      bg2_high =1.5-0.01;
    }
  if(type==1)
    {
      bg1_low = 2.8+0.01;
      bg2_high =3.3-0.01;
    }
  bg1_high = peak_low;
  bg2_low = peak_high;
  


  //TFile* fmixed = new TFile("hhh_Data_reco_tofs9_pvavpr099_mix_binning.root");
  //TFile* fmixed = new TFile("~/sandbox/ReadRecTree_BestInvestigation_normcomb/hhh_Data_reco_tofs9_pvavpr099_mix_finbinning_nw.root");
  //TFile* fmixed = new TFile("~/sandbox/ReadRecTree_BestInvestigation_normcomb/hhh_Data_lifecondition_mix1_and11_finbinning.root");
  //TFile* fmixed = new TFile("~/sandbox/ReadRecTree_BestInvestigation_normcomb/hhh_out_Mixed_Reco_TOFP_resclsdc_reso1wirebdc_05consisbdc_bdclay56_3000tracksize_1pairsdc_wocutinKalclwdt_barid6495bugfixed_NewTofpKillCondTofpCl_Take_allZ1_allZ2_allpi_firstmap_nwpibar_nwpidpr_1bestpion_1enmix11enmix_lifecond_finebinning.root");

  TFile* fmixed = new TFile("./Input/hhh_out_Mixed_Reco_TOFP_resclsdc_reso1wirebdc_05consisbdc_bdclay56_3000tracksize_1pairsdc_wocutinKalclwdt_barid6495bugfixed_NewTofpKillCondTofpCl_Take_allZ1_allZ2_allpi_firstmap_nwpibar_nwpidpr_1bestpion_1enmix11enmix_lifecond_finebinning.root");

  TH1F* hmixed = 0;

  if(type==0)
    {
      //#ifdef Lambda
      hmixed = (TH1F*)fmixed->Get("mother0_all_mix");
      //#endif
    }
  else if(type==1)
    {
      //#ifdef H3L
      hmixed = (TH1F*)fmixed->Get("mother1_all_mix");
      //#endif
    }
  else if(type==2)
    {
      //#ifdef H4L
      hmixed = (TH1F*)fmixed->Get("mother2_all_mix");
      //#endif
    }
  else if(type==3)
    {
      //#ifdef dpi
      hmixed = (TH1F*)fmixed->Get("mother3_all_mix");
      //#endif
    }
  else if(type==4)
    {
      //#ifdef tpi
      hmixed = (TH1F*)fmixed->Get("mother4_all_mix");
      //#endif
    }

  int bin_peak_low = hmixed->GetXaxis()->FindBin(peak_low);
  int bin_peak_high = hmixed->GetXaxis()->FindBin(peak_high);
  int bin_bg1_low = hmixed->GetXaxis()->FindBin(bg1_low);
  int bin_bg1_high = hmixed->GetXaxis()->FindBin(bg1_high);
  int bin_bg2_low = hmixed->GetXaxis()->FindBin(bg2_low);
  int bin_bg2_high = hmixed->GetXaxis()->FindBin(bg2_high);

  double integral_peak = hmixed->Integral(bin_peak_low,bin_peak_high);
  double integral_bg1 = hmixed->Integral(bin_bg1_low,bin_bg1_high);
  double integral_bg2 = hmixed->Integral(bin_bg2_low,bin_bg2_high);

  double norm_bg1 = integral_bg1/(0.5*integral_peak);
  double norm_bg2 = integral_bg2/(0.5*integral_peak);


  if(type==0)
    {
      //#ifdef Lambda
      norm_bg1 = norm_bg1*1.0; // 1.1 phase 0
      norm_bg2 = norm_bg2*1.0; // 1.1
      //#endif
    }
  else if(type ==1)
    {
      //#ifdef H3L
      //norm_bg1 = norm_bg1 * 1.3;//*1.05;//1.05; phase 0
      norm_bg1 = norm_bg1 * 1.3;//*1.05;//1.05; phase 0
      norm_bg2 = norm_bg2 * 1.3;//*1.05;//1.05;
      // #endif
    }
  else if(type==2)
    {
      // #ifdef H4L
      norm_bg1 = norm_bg1*1.05;
      norm_bg2 = norm_bg2*1.05;
      // #endif

      // #ifdef dpi
      //   norm_bg1 = norm_bg1*1.05;
      //   norm_bg2 = norm_bg2*1.05;
      // #endif

      // #ifdef tpi
      //   norm_bg1 = norm_bg1*1.05;
      //   norm_bg2 = norm_bg2*1.05;
      // #endif
    }
  else if(type ==3)
    {
      norm_bg1 = norm_bg1*1.05;
      norm_bg2 = norm_bg2*1.05;

    }
  else if(type == 4)
    {
      norm_bg1 = norm_bg1*1.05;
      norm_bg2 = norm_bg2*1.05;

    }
  

  cout << integral_bg1/(0.5*integral_peak) << " " << integral_bg2/(0.5*integral_peak) << endl;
  cout << 1/norm_bg1 << " " << 1/norm_bg2 << endl;

  TFile* fin = 0;
  
  fin  = TFile::Open(namefile);

  fin->cd();
  TTree* tv__tree = (TTree *) fin->Get("Data_tree");

  Long64_t total_nentries= tv__tree->GetEntries();
  cout<<"N event :"<<total_nentries<<endl;
  Long64_t timing=0; 
  
  Float_t In_InvMass, In_vtx_X,In_vtx_Y,In_vtx_Z,In_DLbetgam,In_tau,In_Eff,In_Ncand,In_Lfactor,In_LExpCorr;
  Int_t In_type,In_SB;
  Int_t In_Nevent;

  tv__tree->SetBranchAddress("InvMass",&In_InvMass);
  tv__tree->SetBranchAddress("vtx_X",&In_vtx_X);
  tv__tree->SetBranchAddress("vtx_Y",&In_vtx_Y);
  tv__tree->SetBranchAddress("vtx_Z",&In_vtx_Z);
  tv__tree->SetBranchAddress("DL_betagamma",&In_DLbetgam);
  tv__tree->SetBranchAddress("Eff",&In_Eff);
  tv__tree->SetBranchAddress("tau",&In_tau);
  tv__tree->SetBranchAddress("Ncand",&In_Ncand);
  tv__tree->SetBranchAddress("Lfactor",&In_Lfactor);
  tv__tree->SetBranchAddress("LExpCorr",&In_LExpCorr);

  tv__tree->SetBranchAddress("Nevent",&In_Nevent);
  tv__tree->SetBranchAddress("type",&In_type);
  tv__tree->SetBranchAddress("SB",&In_SB);


  // RooRealVar decayT("DecayTime","DecayTime",0,1) ;
  // RooRealVar Sc("Sc","Sc",-200,200) ;
  // RooRealVar W("W","W",-1e7,1e7) ;
  // RooRealVar VtxZ("VtxZ","VtxZ",-100,400) ;
  // RooRealVar DeltaL("DeltaL","DeltaL",0,40) ;
  // RooRealVar Mass("InvMass","InvMass",1,5) ;
  
  // RooDataSet h0("dataSB","dataSB",RooArgSet(decayT,Sc,W,VtxZ,DeltaL)) ;
  // RooDataSet b1("dataB1","dataB1",RooArgSet(decayT,Sc,W,VtxZ,DeltaL)) ;
  // RooDataSet b2("dataB2","dataB2",RooArgSet(decayT,Sc,W,VtxZ,DeltaL)) ;
 
  LOldCorr = 9999999;
  

  for(Long64_t i = 0;i<total_nentries;++i)
    {
      tv__tree->GetEntry(i);
      if(i%100000==0)
	std::cout<<"Processing H3L Event#"<<i<<" | "<<(double)i/(double)(total_nentries)*100<<" %"<<std::endl;
      
      if((int)((double)i/(double)(total_nentries)*10)==timing)
	{
	  std::cout<<"Progress H3L:"<<(int)((double)i/(double)(total_nentries)*100.)<<" %"<<std::endl;
	  ++timing; 
	}
      
      bool filling_condition = false;

      if(type==In_type)
	filling_condition = true;


      if(filling_condition )
	{
	  //h_InvMass->Fill(temp_m.M());
	  
	  double temp_lt = In_tau;
	  *decayT = temp_lt;
	  *VtxZ = In_vtx_Z;
	    
	  double temp_delta = 0;
	    
	  double func_eff = 1./In_Eff;
	  
	  if(TMath::Abs(LOldCorr-In_LExpCorr)>1e-7)
	    LOldCorr = In_LExpCorr;

	  if(In_SB >=9)
	    {
	      *W = 1./func_eff*In_Ncand;
	      *Sc = In_Ncand;
	      *DeltaL = temp_delta;
	      *Factor = In_Lfactor;

	      h0->add(RooArgSet(*decayT,*Sc,*W,*VtxZ,*DeltaL,*Factor));
	    }
	      
	  if(In_SB==0)
	    {
	      *W = -1./func_eff/norm_bg1*In_Ncand;
	      *Sc = -1./norm_bg1*In_Ncand;
	      *DeltaL = temp_delta;
	      *Factor = In_Lfactor;

	      b1->add(RooArgSet(*decayT,*Sc,*W,*VtxZ,*DeltaL,*Factor));
	    }
	  if(In_SB==5)
	    {
	      *W = -1./func_eff/norm_bg2*In_Ncand;
	      *Sc = -1./norm_bg2*In_Ncand;
	      *DeltaL = temp_delta;
	      *Factor = In_Lfactor;

	      b2->add(RooArgSet(*decayT,*Sc,*W,*VtxZ,*DeltaL,*Factor));
	    }
	}
    }
	
	    
  cout<<" hist entries h0: "<<h0->numEntries()<<endl;//" "<<h0_1.numEntries()<<" "<<h0_2.numEntries()<<endl;
  cout<<" hist entries b1: "<<b1->numEntries()<<endl;//" "<<b1_1.numEntries()<<" "<<b1_2.numEntries()<<endl;
  cout<<" hist entries b2: "<<b2->numEntries()<<endl;//" "<<b2_1.numEntries()<<" "<<b2_2.numEntries()<<endl;

  
  h0->append(*b1);
  h0->append(*b2);

  

  // RooRealVar NormWeigth("H3LNormWeigth"+nameID,"H3LNormWeigth"+nameID,h0->mean(*Sc)/h0->mean(*W));
  // RooFormulaVar NW("H3LNW"+nameID,"H3LNormWeigth"+nameID+"*H3LW"+nameID,RooArgSet(NormWeigth,*W)) ;
  // RooFormulaVar LW("H3LLW"+nameID,"H3LDecayTime"+nameID+"*H3LW"+nameID,RooArgSet(*decayT,*W)) ;
  // NW_obs = (RooRealVar*) h0->addColumn(NW);
  // LW_obs = (RooRealVar*) h0->addColumn(LW);

  //cout<< wdata_S_exp->mean(decayT) << " "<< NormWeigth.getVal()<<endl;
  //cout<< h0->mean(*LW_obs)*h0->numEntries()<< " "<<h0->mean(*decayT)*h0->numEntries() <<" "<<h0->mean(*W) *h0->numEntries()  <<" "<< NormWeigth.getVal()<<endl;


  // TString temp_cutS("VtxZ>= "),temp_cut2S(" && VtxZ<= "),temp_cut3S(" && DecayTime >= "), temp_cut4S(" && DecayTime <= ");
  // temp_cutS+=z_min;
  // temp_cutS+=temp_cut2S;
  // temp_cutS+=z_max;

  // temp_cutS+=temp_cut3S;
  // temp_cutS+=tau_minCut;
  // temp_cutS+=temp_cut4S;
  // temp_cutS+=tau_maxCut;



}

void H3L_LTFit::SetConditionsCut(double z_min,double z_max, double tau_minCut, double tau_maxCut)
{
  Zmin = z_min;
  Zmax = z_max;
  TauMinCut = tau_minCut;
  TauMaxCut = tau_maxCut;


  // TString temp_cutS("H3LVtxZ>= "),temp_cut2S(" && H3LVtxZ<= "),temp_cut3S(" && H3LDecayTime >= "), temp_cut4S(" && H3LDecayTime <= ");
  // temp_cutS+=z_min;
  // temp_cutS+=temp_cut2S;
  // temp_cutS+=z_max;

  // temp_cutS+=temp_cut3S;
  // temp_cutS+=tau_minCut;
  // temp_cutS+=temp_cut4S;
  // temp_cutS+=tau_maxCut;

  // std::unique_ptr<RooDataSet> tempUnique(dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cutS))));
  // //data_t1_6_exp = std::auto_ptr<RooDataSet>(dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cutS))));
  // //data_t1_6_exp.reset(dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cutS))));
  // //data_t1_6_exp = dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cutS)));
  // data_t1_6_exp = std::move(tempUnique);
}


double H3L_LTFit::CalculateLogLikelihood(const std::vector<double>& parameters)
{

  //double tauLambda = parameters[0];
  double tauH3L    = parameters[1];
  // double CorrEff   = parameters[2];
  // double TauMin    = parameters[3];
  // double TauMax    = parameters[4];

  double CorrEff   = parameters[2];
  //double CorrNi    = parameters[3];
  // double CorrRho   = parameters[4];
  // double CorrDelt  = parameters[5];
  double TauMin    = parameters[3];
  double TauMax    = parameters[4];


  this->SetConditionsCut(150.,300.,TauMin,TauMax);

  std::unordered_map<std::vector<double>, double, HashVec>::const_iterator it_cache = cache_res.find(parameters);
  if(it_cache != cache_res.end())
    return it_cache->second;


  // TString tempSign[2]={" >= "," <= "};
  // TString temp_cutS("H3LVtxZ"),temp_cut2S(" && H3LVtxZ"),temp_cut3S(" && H3LDecayTime"), temp_cut4S(" && H3LDecayTime");
  // temp_cutS+=ID; temp_cutS+=tempSign[0];
  // temp_cut2S+=ID; temp_cut2S+=tempSign[1];
  // temp_cut3S+=ID; temp_cut3S+=tempSign[0];
  // temp_cut4S+=ID; temp_cut4S+=tempSign[1];

  // temp_cutS+=Zmin;
  // temp_cutS+=temp_cut2S;
  // temp_cutS+=Zmax;

  // temp_cutS+=temp_cut3S;
  // temp_cutS+=TauMinCut;
  // temp_cutS+=temp_cut4S;
  // temp_cutS+=TauMaxCut;

  //std::unique_ptr<RooDataSet> tempUnique(dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cutS))));
  //RooDataSet* tempUnique = (dynamic_cast<RooDataSet*>(h0->reduce(Cut( temp_cutS))));
  
  const RooArgSet* obsH3L = h0->get() ;

  RooRealVar* DTdata = dynamic_cast<RooRealVar*>(obsH3L->find(this->decayT->GetName())) ;
  RooRealVar* Ndata  = dynamic_cast<RooRealVar*>(obsH3L->find(this->Sc->GetName())) ;
  RooRealVar* Wdata  = dynamic_cast<RooRealVar*>(obsH3L->find(this->W->GetName())) ;
  RooRealVar* vtxZdata  = dynamic_cast<RooRealVar*>(obsH3L->find(this->VtxZ->GetName())) ;
  
  double sum_wi = 0.;
  double sum_witi = 0.;
  double normalizeW = 0.;
       
  for(int i=0 ; i<h0->numEntries();++i)
    {
      h0->get(i);
      double vtx_zi = vtxZdata->getVal(); 
      double ti = DTdata->getVal();
      if(Zmin <= vtx_zi && vtx_zi <= Zmax && TauMinCut <= ti && ti <= TauMaxCut)  
	{
	  double wi = Wdata->getVal();
	  double ni = Ndata->getVal();
	  
	  double corr_i = exp(-ti/CorrEff)/(CorrEff*(exp(-TauMinCut/CorrEff)-exp(-TauMaxCut/CorrEff)));
	  //double corr_i = TMath::Exp(-ti/CorrEff-ni/CorrNi);//-CorrRho*TMath::Max(ti,ni+TMath::Min(ti,CorrDelt))); 
	  double new_wi = wi*corr_i;
	   
	  sum_witi += ti*new_wi;
	  sum_wi += new_wi;

	  normalizeW += ni;
	}
    }
  normalizeW /= sum_wi;

  double LogL_H3L = normalizeW*( sum_wi*(-log(tauH3L) - log( exp(-TauMinCut/tauH3L) -exp(-TauMaxCut/tauH3L) ) ) - sum_witi/tauH3L);

  cache_res.insert(std::pair<std::vector<double>, double>(parameters,LogL_H3L));

  return LogL_H3L;
  
}
