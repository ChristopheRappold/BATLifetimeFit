#ifndef __DATFACTORY__H
#define __DATFACTORY__H

#include <iostream>
#include <map>
#include <string>
#include <limits>
#include <memory>
#include <vector>
#include <list>
#include <algorithm>


#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"

#include "TMath.h"


#include "RooMsgService.h"
#include "RooWorkspace.h"
#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooFitResult.h"
#include "RooFormulaVar.h"

#include "Riostream.h"

#include "TF1.h"
#include "TH1F.h"
#include "TRandom3.h"


#include <unordered_map>
#include <functional>

using namespace RooFit ;


class HashVec
{
  std::hash<std::string> hashFunc;
public :
  size_t operator() (const std::vector<double>& a) const
  {
    int temp[] = {TMath::Nint(a[0]*1.e4), TMath::Nint(a[1]*1.e4), 
		  TMath::Nint(a[2]*1.e5), //TMath::Nint(a[3]*1.e5), //TMath::Nint(a[4]*1.e5), TMath::Nint(a[5]*1.e5), 
		  TMath::Nint(a[3]*1.e3), TMath::Nint(a[4]*1.e3)};
    std::string nameHash("");
    for( auto& i : temp )
      {
	std::string nameHashTemp = std::to_string(i);
	nameHash += nameHashTemp;
      }
    return hashFunc(nameHash);
  }

};

class Lambda_LTFit
{

public:
  const TString& nameID;
  const int ID;

  RooRealVar* decayT ;
  RooRealVar* Sc ;
  RooRealVar* W;
  RooRealVar* VtxZ;
  RooRealVar* DeltaL;
  RooRealVar* Mass;
  RooRealVar* Factor;
  
  // RooRealVar* NW_obs;
  // RooRealVar* LW_obs;


  RooDataSet* h0;
  RooDataSet* b1;
  RooDataSet* b2;

  // TF1* LogL;
  // TRandom3* rand;

  double Zmin;
  double Zmax;
  double TauMinCut;
  double TauMaxCut;

  std::unordered_map<std::vector<double>, double, HashVec> cache_res;

  //std::unique_ptr<RooDataSet> data_t1_6_exp;

  Lambda_LTFit(const TString& i, int id);
  Lambda_LTFit(const Lambda_LTFit& data1);

  // void processFit (double z_min,double z_max, double tau_minCut, double tau_maxCut, std::vector<double>& fittedValues, double NewCalibCorrSys = 1.);
  // int processFactor (double z_min,double z_max, double tau_minCut, double tau_maxCut, std::vector<double>& resValues);
  void Init(const TString& namefile);
  void SetConditionsCut(double z_min,double z_max, double tau_minCut, double tau_maxCut);
  double CalculateLogLikelihood(const std::vector<double>& pars); 
  void Print() const;

};




class H3L_LTFit
{

public:
  const TString& nameID;
  const int ID;

  RooRealVar* decayT ;
  RooRealVar* Sc ;
  RooRealVar* W;
  RooRealVar* VtxZ;
  RooRealVar* DeltaL;
  RooRealVar* Mass;
  RooRealVar* Factor;
  
  // RooRealVar* NW_obs;
  // RooRealVar* LW_obs;


  RooDataSet* h0;
  RooDataSet* b1;
  RooDataSet* b2;

  // TF1* LogL;
  // TRandom3* rand;

  double LOldCorr ;


  double Zmin;
  double Zmax;
  double TauMinCut;
  double TauMaxCut;

  std::unordered_map<std::vector<double>, double, HashVec> cache_res;

  //std::auto_ptr<RooDataSet> data_t1_6_exp;
  //std::unique_ptr<RooDataSet> data_t1_6_exp;

  H3L_LTFit(const TString& i, int id);
  H3L_LTFit(const H3L_LTFit& data2);
  //void processFit (double z_min,double z_max, double tau_minCut, double tau_maxCut, std::vector<double>& fittedValues, double NewCalibCorrSys = 1.);
  void Init(const TString& namefile,int type=1);
  void SetConditionsCut(double z_min,double z_max, double tau_minCut, double tau_maxCut);
  double CalculateLogLikelihood(const std::vector<double>& pars); 
  void Print() const;

};

#endif 
